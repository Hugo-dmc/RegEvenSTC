﻿using ReportSTCVersion01.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ReportSTCVersion01
{
    public partial class MainPage : MasterDetailPage
    {
        public List<MasterPageItem> menuList { get; set; }

        public MainPage()
        {

            InitializeComponent();

            menuList = new List<MasterPageItem>();

            // Creating our pages for menu navigation
            // Here you can define title for item, 
            // icon on the left side, and page that you want to open after selection
            var page0 = new MasterPageItem() { Title = "Inicio", Icon = "home.png", TargetType = typeof(VistaModelo.Principal) };

            var page1 = new MasterPageItem() { Title = "Perfil", Icon = "perfil.png", TargetType = typeof(VistaModelo.Perfil) };
            var page2 = new MasterPageItem() { Title = "Ayuda", Icon = "ayuda.png", TargetType = typeof(VistaModelo.Ayuda) };
            var page3 = new MasterPageItem() { Title = "Soporte", Icon = "soporte.png", TargetType = typeof(VistaModelo.Soporte) };
            var page4 = new MasterPageItem() { Title = "Acerca de", Icon = "info.png", TargetType = typeof(VistaModelo.AcercaDe) };
            var page5 = new MasterPageItem() { Title = "prueba reporte", Icon = "", TargetType = typeof(VistaModelo.EnviarReporte) };
            var page6 = new MasterPageItem() { Title = "cambia contraseña", Icon = "", TargetType = typeof(VistaModelo.Contrasenia) };


            // Adding menu items to menuList
            menuList.Add(page0);
            menuList.Add(page1);

            menuList.Add(page2);
            menuList.Add(page3);
            menuList.Add(page4);
            menuList.Add(page5);
            //menuList.Add(page6);


            // Setting our list to be ItemSource for ListView in MainPage.xaml
            navigationDrawerList.ItemsSource = menuList;

            // Initial navigation, this can be used for our home page
            Detail = new NavigationPage((Page)Activator.CreateInstance(typeof(VistaModelo.Principal)));

        }

        // Event for Menu Item selection, here we are going to handle navigation based
        // on user selection in menu ListView
        private void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

            var item = (MasterPageItem)e.SelectedItem;
            Type page = item.TargetType;

            Detail = new NavigationPage((Page)Activator.CreateInstance(page));
            IsPresented = false;
        }
    }
}
