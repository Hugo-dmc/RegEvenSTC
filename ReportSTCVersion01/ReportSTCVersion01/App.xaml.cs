﻿using ReportSTCVersion01.Modelo;
using Xamarin.Forms;

namespace ReportSTCVersion01
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            var  db = new Database();
            db.CargaEstaciones();
            MainPage = new ReportSTCVersion01.MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}