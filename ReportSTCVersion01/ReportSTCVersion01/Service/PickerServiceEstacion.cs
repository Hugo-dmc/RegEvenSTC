﻿using ReportSTCVersion01.Modelo;
using System.Collections.Generic;

namespace ReportSTCVersion01.ViewModel
{
    public class PickerServiceEstacion
    {        
        public static List<CatalogoEstacion> GetEstaciones(string estacion)
        {
            Database db = new Database();
            var estaciones = new List<CatalogoEstacion>();
            estaciones = db.GetEstaciones(estacion);
            return estaciones;
        }
    }
}