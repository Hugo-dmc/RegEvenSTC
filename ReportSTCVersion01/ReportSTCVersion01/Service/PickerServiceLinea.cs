﻿using ReportSTCVersion01.Modelo;
using System.Collections.Generic;

namespace ReportSTCVersion01.Service
{
    public class PickerServiceLinea
    {
        public static List<CatalogoLinea> GetCities()
        {
            Database db = new Database();
            var lineas = new List<CatalogoLinea>();
            lineas = db.GetLineas();
            return lineas;
        }
    }
}