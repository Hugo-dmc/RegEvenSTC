﻿using SQLite.Net.Attributes;

namespace ReportSTCVersion01.Modelo
{
    [Table("RelLineaEstacion")]
    public class RelLineaEstacion
    {
        [PrimaryKey]
        [Column("IdLineaEstacion")]
        public int IdLineaEstacion { get; set; }

        [Column("IdRelLinea")]
        public int IdRelLinea { get; set; }

        [Column("IdRelEstacion")]
        public int IdRelEstacion { get; set; }
    }
}