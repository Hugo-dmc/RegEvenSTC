﻿using SQLite.Net.Attributes;

namespace ReportSTCVersion01.Modelo
{
    [Table("CatalogoLinea")]
    public class CatalogoLinea
    {
        [PrimaryKey]
        [Column("IdLinea")]
        public int IdLinea { get; set; }

        [Column("NombreLinea")]
        public string NombreLinea { get; set; }

        [Column("NombreZona")]
        public string NombreZona { get; set; }
    }
}