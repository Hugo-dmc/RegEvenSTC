﻿using ReportSTCVersion01.Helpers;
using ReportSTCVersion01.Modelo;
using ReportSTCVersion01.VistaModelo;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace ReportSTCVersion01.ViewModel
{
    public class LoginPageViewModel : ViewModelBase
    {
        private Database db;
        #region Commands
        public INavigation Navigation { get; set; }
        public ICommand LoginCommand { get; set; }
        #endregion

        #region Properties
        
        private Usuario _usuario = new Usuario();

        public Usuario Usuario
        {
            get { return _usuario; }
            set { SetProperty(ref _usuario, value); }
        }

        private string _message;

        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }
        #endregion

        public LoginPageViewModel()
        {
            db = new Database();
            LoginCommand = new Command(Login);
        }
        public async void Login()
        {
            IsBusy = true;
            Title = string.Empty;
            try
            {                
                if (Usuario.NumEmpleado != null)
                    {
                    if (Usuario.Password != null)
                    {
                        Usuario u = new Usuario();
                        u = db.GetUsuario(Usuario.NumEmpleado,Usuario.Password);
                        if(Usuario.NumEmpleado == u.NumEmpleado &&
                            Usuario.Password ==  u.Password)
                        {
                            Settings.IsLoggedIn = true;
                            await Navigation.PushAsync(new EnviarReporte(u.NumEmpleado));
                        }
                        else
                        {
                            Message = "Usuario o contraseña incorrecta";
                        }
                        IsBusy = false;
                    }
                    else
                    {
                        IsBusy = false;
                        Message = "La contraseña es requerida";
                    }
                }
                else
                {
                    IsBusy = false;
                    Message = "El email es requerido";
                }
            }
            catch (Exception e)
            {
                IsBusy = false;
                await App.Current.MainPage.DisplayAlert("Error de conexión", e.Message, "Ok");
            }
        }
    }
}