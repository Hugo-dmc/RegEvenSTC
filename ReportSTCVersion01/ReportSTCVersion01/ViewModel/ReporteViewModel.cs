﻿using ReportSTCVersion01.Modelo;
using ReportSTCVersion01.Service;
using System.Collections.Generic;
using System.Linq;

namespace ReportSTCVersion01.ViewModel
{
    public class ReporteViewModel: BaseViewModel
    {        
        public List<CatalogoLinea> ListLineas
        {
            get;
            set;
        }
        public List<TipoIncidencia> ListTipoIncidencia
        {
            get;
            set;
        }
        public ReporteViewModel()
        {            
            ListLineas = PickerServiceLinea.GetCities().OrderBy(
                c => c.NombreLinea).ToList();
            
            ListTipoIncidencia = PickerService.GetCities().OrderBy(
                c => c.NombreTipoIncidencia).ToList();            
        }

        private CatalogoLinea _selectedLinea;
        public CatalogoLinea SelectedLinea
        {

            get { return _selectedLinea; }
            set
            {                
                SetProperty(ref _selectedLinea, value);
                CityText = "Zona : " + _selectedLinea.NombreZona;                              
            }
        }       

        private string _cityText;
        public string CityText
        {
            get { return _cityText; }
            set
            {
                SetProperty(ref _cityText, value);
            }
        }                               
    }
}
